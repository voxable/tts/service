<div align="center">
  <a href="https://gitlab.com/voxable/tts" target="blank"><img src="https://p129.p0.n0.cdn.getcloudapp.com/items/WnuN2PKn/default.png?v=aa715847410dbe6d5b2c19bbb072fe40" width="320" alt="TTS Logo" /></a>
  <p>One API call. Many voices.</p>
</div>

## Description

`tts-service` is a pluggable back-end service for synthesizing speech from a variety of TTS services via a unified API.

## Stack

* [Nest](https://github.com/nestjs/nest)
* TypeScript
* Jest

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Support

`tts-service` is an MIT-licensed open source project. 

## Stay in touch

Come [say howdy to Voxable](https://voxable.io/contact).

## License

`tts-server` is [MIT licensed](LICENSE).
